
import './App.css';
import Header from "./component/header";
import Services from "./component/services";
import Design from "./component/design";
import Team from "./component/team";
import Testimonial from "./component/testimonial";
import Companys from "./component/companys";
import Blog from "./component/blog";
import Footer from "./component/footer";
import BottomFooter from "./component/bottomFooter";

function App() {
  return (
    <div className="App">
        <Header/>
        <Services/>
        <Design/>
        <Team/>
        <Testimonial/>
        <Companys/>
        <Blog/>
        <Footer/>
        <BottomFooter/>
    </div>
  );
}

export default App;
