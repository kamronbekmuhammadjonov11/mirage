import React from 'react';
import "../styles/testimonial.scss";

function Testimonial(props) {
    return (
        <div className="testimonial">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <img src="images/Image Section.png" alt="no image"/>
                    </div>
                    <div className="col-md-6">
                        <h5>Testimonial </h5>
                        <h2>Thats What Our
                            Client Says About Us</h2>
                        <p>“ Heartiest congratulations to the team for <br/> Future projects. I extend thankful wishes and<br/>
                            wish the team best of luck for the future <br/> assignments to come. Your effort is gladly ”</p>
                        <h5 className="name">Jorge Morisson</h5>
                        <div className="testimonialFooter">
                            <div className="address">
                                North California,USA
                            </div>
                            <div className="arrowIcons">
                                <div className="arrowLeftIcon">
                                    <img src="images/Group.svg" alt="no image"/>
                                </div>
                                <div className="arrowRightIcon">
                                    <img src="images/Group (1).svg" alt="no image"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Testimonial;