import "../styles/header.scss"

function Header() {
    return <div className="header">
        <div className="positionImg">
            <img src="images/Bloobs.png" alt="no image"/>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="navbar navbar-expand-md">
                        <div className="navbar-brand ">
                            <div className="imdSize">
                                <a href="#">
                                    <img src="/images/Mirage.svg" alt="no images"/>
                                </a>
                            </div>
                        </div>
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a href="#" className="nav-link">Home</a>
                            </li>
                            <li className="nav-item">
                                <a href="#" className="nav-link">About</a>
                            </li>
                            <li className="nav-item">
                                <a href="#" className="nav-link">Product</a>
                            </li>
                            <li className="nav-item">
                                <a href="#" className="nav-link">Pricing</a>
                            </li>
                        </ul>
                        <div className="navbar-icon">
                            <img src="/images/Vector (5).svg" alt="no images"/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="creative">
                        <div className="line">

                        </div>
                        <div className="creativeText">
                            Creative Agency
                        </div>
                    </div>
                    <h1> We Deliver The Best<br/>
                       Product for You</h1>
                    <p>Design is the creation of a plan or convention for the <br/> construction of an object or a system as
                        in architectural <br/> blueprints, engineering drawings, business processes,</p>
                    <button className="button">
                        Get started
                        <span className="buttonIcon">
                            <span className="arrow">
                                <img src="images/Vector 2 (1).svg" alt="no images"/>
                            </span>
                        </span>
                    </button>
                </div>
                <div className="col-md-6">
                    <img src="images/Download 1.png" alt="no image"/>
                </div>
            </div>
        </div>
    </div>
}

export default Header