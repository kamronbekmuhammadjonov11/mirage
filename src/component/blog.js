import React from 'react';
import "../styles/blog.scss";
function Blog(props) {
    return (
        <div className="blog">
            <div className="backgroundLeftImg">
                <img src="images/Mail Illustration.png" alt="no images"/>
            </div>
            <div className="backgroundRightImg">
                <img src="images/Bloobs (2).png" alt="no images"/>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-8 offset-2">
                        <h2>Subscribe To Our Blog</h2>
                        <p>A newsletter is a tool used to communicate regularly with<br/>
                            your subscribers, delivering the information</p>
                        <div className="input-group">
                            <input type="text" placeholder="Type Your Email Address"/>
                            <button className="learnButton">
                                Send now
                                <span className="buttonIcon">
                                    <span className="arrow">
                                        <img src="images/Vector 2 (1).svg" alt="no images"/>
                                    </span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Blog;