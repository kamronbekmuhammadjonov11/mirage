import React from 'react';
import "../styles/companys.scss";
function Companys(props) {
    return (
        <div className="companys">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                       <div className="companysGroup">
                           <div className="companysItem">
                               <img src="images/11.svg" alt="no imagws"/>
                           </div>
                           <div className="companysItem">
                               <img src="images/Group (3).svg" alt="no imagws"/>
                           </div>
                           <div className="companysItem">
                               <img src="images/g3371_1_.svg" alt="no imagws"/>
                           </div>
                           <div className="companysItem">
                               <img src="images/Group 24.svg" alt="no imagws"/>
                           </div>
                           <div className="companysItem">
                               <img src="images/5c7120fa2d05cec571969c2d_google-logo-export-v1 1.png" alt="no imagws"/>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Companys;