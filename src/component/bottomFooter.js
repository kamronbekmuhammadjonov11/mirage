import React from 'react';
import "../styles/bottomFoter.scss";
function BottomFooter(props) {
    return (
        <div className="bottomFooter">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="footerLeft">
                            <img src="images/Vector (13).svg" alt="no  images"/>
                            All Rights Reserved By Mirage 2020
                        </div>
                        <div className="footerRight">
                            <img src="images/Vector (14).svg" alt="no  images"/>
                            <select >
                                <option value="eng">Eng</option>
                                <option value="uz">Uzb</option>
                                <option value="ru">Ru</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BottomFooter;